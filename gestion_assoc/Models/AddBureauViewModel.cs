﻿namespace gestion_assoc.Models
{
    public class AddBureauViewModel
    {
        private Guid id { get; set; }
        private string nom { get; set; }
        private DateOnly periode_debut { get; set; }
        private string duree { get; set; }
    }
}
