﻿namespace gestion_assoc.Models
{
    public class AddMembreViewModel
    {
        public Guid id { get; set; }
        public string nom { get; set; }
        public string prenom { get; set; }
        public DateTime date_naiss { get; set; }
        public string lieu_naiss { get; set; }
    }
}
