﻿namespace gestion_assoc.Models.Domain
{
    public class Activites
    {
        public Guid id { get; set; }
        public string libelle { get; set; }
        public string type_activite { get; set; }
        public DateTime date_debut { get; set; }
        public DateTime date_fin { get; set; }
        public string besoin { get; set; }
        public string lieu { get; set; }
    }
}
