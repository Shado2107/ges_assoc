﻿using gestion_assoc.Data;
using gestion_assoc.Models.Domain;
using gestion_assoc.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace gestion_assoc.Controllers
{
    using Data;
    public class MemberController : Controller
    {
        private readonly GestionAssocDbContext gestionAssocDbContext;

        public MemberController(GestionAssocDbContext gestionAssocDbContext)
        {
            this.gestionAssocDbContext = gestionAssocDbContext;
        }
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> Create(AddMembreViewModel addMembreRequest)
        {
            var membre = new Membre()
            {
                id = Guid.NewGuid(),
                nom = addMembreRequest.nom,
                prenom = addMembreRequest.prenom,
                date_naiss= addMembreRequest.date_naiss,
                lieu_naiss = addMembreRequest.lieu_naiss,
               
            };

            await gestionAssocDbContext.membres.AddAsync(membre);
            await gestionAssocDbContext.SaveChangesAsync();
            return RedirectToAction("List");

        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            var membres =await gestionAssocDbContext.membres.ToListAsync();
            return View(membres);
        }

        public async Task<IActionResult> Read(Guid id)
        {
            var membre = await gestionAssocDbContext.membres.FirstOrDefaultAsync(x => x.id == id);
            if (membre != null)
            {
                var viewModel = new UpdateMembreViewModel
                {
                    id = membre.id,
                    nom = membre.nom,
                    prenom = membre.prenom,
                    date_naiss = membre.date_naiss,
                    lieu_naiss = membre.lieu_naiss
                    
                };

                return await Task.Run(() => View("Read", viewModel));
            }
            return RedirectToAction("List");
        }


        [HttpPost]
        public async Task<IActionResult> Read(UpdateMembreViewModel model)
        {
            var membre = await gestionAssocDbContext.membres.FindAsync(model.id);
            if (membre != null)
            {
                membre.nom = model.nom;
                membre.prenom = model.prenom;
                membre.date_naiss = model.date_naiss;
                membre.lieu_naiss = model.lieu_naiss;             
                await gestionAssocDbContext.SaveChangesAsync();
                return RedirectToAction("List");
            }

            return RedirectToAction("List");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(UpdateMembreViewModel model)
        {
            var membre = await gestionAssocDbContext.membres.FindAsync(model.id);

            if (membre!= null)
            {
                gestionAssocDbContext.membres.Remove(membre);
                await gestionAssocDbContext.SaveChangesAsync();

                return RedirectToAction("List");
            }

            return RedirectToAction("List");
        }
    }
}
