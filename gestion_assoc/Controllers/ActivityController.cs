﻿using gestion_assoc.Data;
using gestion_assoc.Models.Domain;
using gestion_assoc.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace gestion_assoc.Controllers
{
    public class ActivityController : Controller
    {

        private readonly GestionAssocDbContext gestionAssocDbContext;

        public ActivityController(GestionAssocDbContext gestionAssocDbContext)
        {
            this.gestionAssocDbContext = gestionAssocDbContext;
        }

        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> Create(AddActivitesViewModel addActivityRequest)
        {
            var activity = new Activites()
            {
                id = Guid.NewGuid(),
                libelle = addActivityRequest.libelle,
                type_activite= addActivityRequest.type_activite,
                date_debut = addActivityRequest.date_debut,
                date_fin = addActivityRequest.date_fin,
                besoin = addActivityRequest.besoin,
                lieu = addActivityRequest.lieu,
            };

            await gestionAssocDbContext.Activites.AddAsync(activity);
            await gestionAssocDbContext.SaveChangesAsync();
            return RedirectToAction("List");

        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            var activites = await gestionAssocDbContext.Activites.ToListAsync();
            return View(activites);
        }


        public IActionResult Update()
        {
            return View();
        }

        public IActionResult Show()
        {
            return View();
        }

    }
}
