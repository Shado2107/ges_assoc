﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace gestion_assoc.Migrations
{
    public partial class Activites : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Activites",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    libelle = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    type_activite = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    date_debut = table.Column<DateTime>(type: "datetime2", nullable: false),
                    date_fin = table.Column<DateTime>(type: "datetime2", nullable: false),
                    besoin = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    lieu = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activites", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Activites");
        }
    }
}
